﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicExercises
{

    /// <summary>
    ///     This class exsists for the sole purpose of practicing basic C# functionality
    ///     
    ///     I feel as if I have gotten a little rusty and need to sharpen my skills against
    ///     event the simplest of tests
    ///     
    ///     Questions can be found: https://www.w3resource.com/csharp-exercises/basic/index.php#editorr
    ///     
    ///     I do my best to answer simple questions in interesting ways, as opposed to the most straight
    ///     forward answer, using some of the tools available in C#
    /// </summary>

    class Program
    {

        public delegate void QuestionDelegate();

        public static void RunQuestion( string question, QuestionDelegate qd )
        {
            Console.WriteLine("*********\t" + question + "\t******** \n");
            qd();
            Console.WriteLine();

        }

        /// <summary>
        /// 1. Write a C# Sharp program to print Hello and your name in a separate line. Go to the editor
        // Expected Output : 
        // Hello: Alexandra Abramov
        /// </summary>
        public static void Q1()
        {
            Console.WriteLine("Hello: Dustin");
        }

        /// <summary>
        /// 2. Write a C# Sharp program to print the sum of two numbers.
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns></returns>
        public static void Q2()
        {
            double num1 = 11.0;
            double num2 = 10.0;
            double sum = Program.Sum(num1, num2);
            string output = String.Format("num1: {0} \nnum2: {1} \nSum: {2}", num1, num2, sum);
            Console.WriteLine(output);
        } 

        public static double Sum( double num1, double num2)
        {
            return num1 + num2;
        }

        /// <summary>
        /// Write a C# Sharp program to print the result of dividing two numbers.
        /// </summary>
        public static void Q3()
        {
            double num1 = 11.0;
            double num2 = 10.0;
            double div = Program.Divide(num1, num2);
            string output = String.Format("num1: {0} \nnum2: {1} \nDivide: {2}", num1, num2, div);
            Console.WriteLine(output);
        }

        public static double Divide(double num1, double num2)
        {
            return num1 / num2;
        }

        /// <summary>
        /// 4. Write a C# Sharp program to print the result of the specified operations. Go to the editor
        /// Test data:
        /// -1 + 4 * 6 
        /// ( 35+ 5 ) % 7 
        /// 14 + -4 * 6 / 11 
        /// 2 + 15 / 6 * 1 - 7 % 2 
        /// Expected Output:
        /// 23
        /// 5
        /// 12
        /// 3
        /// </summary>
        public static void Q4()
        {
            double num1 = -1 + 4 * 6;
            double num2 = (35 + 5) % 7;
            double num3 = 14 + -4 * 6 / 11;
            double num4 = 2 + 15 / 6 * 1 - 7 % 2;
            string output = String.Format("{0}\n{1}\n{2}\n{3}\n", num1, num2, num3, num4);
            Console.WriteLine(output);
        }

        /// <summary>
        /// 5. Write a C# Sharp program to swap two numbers.
        /// </summary>
        public static void Q5()
        {
            Tuple<double, double> twoNumbers = new Tuple<double, double>(10.1, 11.0);
            Console.WriteLine(twoNumbers);
            twoNumbers = Program.Swap(twoNumbers);
            Console.WriteLine(twoNumbers);
        }

        public static Tuple<double, double> Swap( Tuple<double, double> twoNumbers )
        {
            return new Tuple<double, double>(twoNumbers.Item2, twoNumbers.Item1);
        }

        /// <summary>
        /// 6. Write a C# Sharp program to print the output of multiplication of three 
        /// numbers which will be entered by the user
        /// </summary>
        public static void Q6()
        {
            int num1, num2, num3;
            Console.WriteLine("Enter a number");
            bool num1State = Int32.TryParse(Console.ReadLine(), out num1);
            Console.WriteLine("Enter a number");
            bool num2State = Int32.TryParse(Console.ReadLine(), out num2);
            Console.WriteLine("Enter a number");
            bool num3State = Int32.TryParse(Console.ReadLine(), out num3);

            if ( num1State && num2State && num3State )
            {
                Console.WriteLine(num1 * num2 * num3);
            } else {
                Console.WriteLine("One of the items entered was not a number");
            }

        }

        /// <summary>
        /// 8. Write a C# Sharp program that takes a number as input and 
        /// print its multiplication table
        /// </summary>
        public static void Q8()
        {
            int number = 9;
            int multiplicationTableMax = 12;

            for (int i = 0; i < multiplicationTableMax; i++)
            {
                Console.WriteLine("{0} * {1} = {2}", number, ++i, number * i--);
            }

        }

        /// <summary>
        /// 15. Write a C# program remove specified a character from a non-empty 
        /// string using index of a character.
        /// </summary>
        public static void Q15()
        {
            string dustin = "dustin", brandon = "brandon", christina = "christina";
            Console.WriteLine(Program.RemoveLetter(dustin, 2));
            Console.WriteLine(Program.RemoveLetter(brandon, 0));
            Console.WriteLine(Program.RemoveLetter(christina, 8));
        }

        public static string RemoveLetter( string s, int index )
        {
            if (index == 0)
            {
                return s.Substring(index + 1);
            } else if (index == s.Length - 1) {
                return s.Substring(0, index);
            } else if (index < s.Length ) {
                return s.Substring(0, index) + s.Substring(index + 1);
            } else {
                throw new IndexOutOfRangeException("The index passed in is outside the length of the word");
            }

        }

        /// <summary>
        /// 26. Write a C# program to compute the sum of the first 500 prime numbers
        /// </summary>
        public static void Q26()
        {
            int primeNumberCount = 500;
            int[] primeNumbers = new int[primeNumberCount];

            for (int i = 1 ; primeNumberCount != 0 ; i++)
            {

                bool isPrime = true;

                for (int j = 1; j <= i && isPrime ; j++)
                {
                    if ( i % j == 0 && i != 1 && i != j)
                    {
                        isPrime = false;
                    }
                }

                if (isPrime)
                {
                    primeNumbers[--primeNumberCount] = i;
                }

            }

            foreach (int i in primeNumbers)
            {
                Console.WriteLine(i);
            }

        }

        /// <summary>
        /// Run the questions
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            RunQuestion("Question 1", Program.Q1);
            RunQuestion("Question 2", Program.Q2);
            RunQuestion("Question 3", Program.Q3);
            RunQuestion("Question 4", Program.Q4);
            RunQuestion("Question 5", Program.Q5);
            // RunQuestion("Question 6", Program.Q6); // Requires user Input
            RunQuestion("Question 8", Program.Q8);
            RunQuestion("Question 15", Program.Q15);
            RunQuestion("Question 26", Program.Q26);
        }
    }
}
